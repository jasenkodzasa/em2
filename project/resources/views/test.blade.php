<h1>User Roles</h1>
<p>{{\Illuminate\Support\Facades\Auth::user()->roles}}</p>

<h1>Assigned Abilities</h1>
{{--@can("create", \App\User::class)--}}
    {{--12--}}
{{--@endcan--}}
@foreach(\Illuminate\Support\Facades\Auth::user()->getAbilities() as $ability)
    <p>{{$ability}}</p>
@endforeach
@can("delete", \App\User::class)
    12
@endcan
@can("create")
    13
@endcan
@can("edit", \Illuminate\Support\Facades\Auth::user())
    14
@endcan
<h1>All Abilities</h1>
@foreach(\Illuminate\Support\Facades\DB::table("abilities")->get() as $ab)
    <p>{{$ab->name}} {{$ab->entity_id}} {{$ab->entity_type}} </p>
@endforeach