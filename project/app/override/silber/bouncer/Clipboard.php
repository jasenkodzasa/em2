<?php

namespace Silber\Bouncer;

use Illuminate\Support\Facades\DB;
use Silber\Bouncer\Database\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;

class Clipboard
{
    use HandlesAuthorization;

    /**
     * Whether the bouncer is the exclusive authority on gate access.
     *
     * @var bool
     */
    protected $exclusive = false;

    /**
     * Register the clipboard at the given gate.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function registerAt(Gate $gate)
    {
        $gate->before(function ($user, $ability, $arguments = [], $additional = null) {

            list($model, $additional) = $this->parseGateArguments($arguments, $additional);

            if ( ! is_null($additional)) {
                return;
            }


            if ($id = $this->checkGetId($user, $ability, $model)) {
                return $this->allow('Bouncer granted permission via ability #'.$id);
            } else{
                $this->checkAndDefineAbility($user, $ability, $arguments);
            }

            if ($this->exclusive) {
                return false;
            }
        });
    }

    /**
     * Parse the arguments we got from the gate.
     *
     * @param  mixed  $arguments
     * @param  mixed  $additional
     * @return array
     */
    protected function parseGateArguments($arguments, $additional)
    {
        // The way arguments are passed into the gate's before callback has changed in Laravel
        // in the middle of the 5.2 release. Before, arguments were spread out. Now they're
        // all supplied in a single array instead. We will normalize it into two values.
        if ( ! is_null($additional)) {
            return [$arguments, $additional];
        }

        if (is_array($arguments)) {
            return [
                isset($arguments[0]) ? $arguments[0] : null,
                isset($arguments[1]) ? $arguments[1] : null,
            ];
        }

        return [$arguments, null];
    }

    /**
     * Set whether the bouncer is the exclusive authority on gate access.
     *
     * @param  bool  $boolean
     * @return $this
     */
    public function setExclusivity($boolean)
    {
        $this->exclusive = $boolean;
    }

    /**
     * Determine if the given user has the given ability.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $user
     * @param  string  $ability
     * @param  \Illuminate\Database\Eloquent\Model|string|null  $model
     * @return bool
     */
    public function check(Model $user, $ability, $model = null)
    {
        return (bool) $this->checkGetId($user, $ability, $model);
    }

    /**
     * Determine if the given user has the given ability and return the ability ID.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $user
     * @param  string  $ability
     * @param  \Illuminate\Database\Eloquent\Model|string|null  $model
     * @return int|bool
     */
    protected function checkGetId(Model $user, $ability, $model = null)
    {
        $abilities = $this->getAbilities($user)->toBase()->lists('identifier', 'id');

        $requested = $this->compileAbilityIdentifiers($ability, $model);

        foreach ($abilities as $id => $ability) {
            if (in_array($ability, $requested)) {
                return $id;
            }
        }

        return false;
    }

    /**
     * Check if a user has the given roles.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $user
     * @param  array|string  $roles
     * @param  string  $boolean
     * @return bool
     */
    public function checkRole(Model $user, $roles, $boolean = 'or')
    {
        $available = $this->getRoles($user)->intersect($roles);

        if ($boolean == 'or') {
            return $available->count() > 0;
        } elseif ($boolean === 'not') {
            return $available->count() === 0;
        }

        return $available->count() == count((array) $roles);
    }

    /**
     * Compile a list of ability identifiers that match the provided parameters.
     *
     * @param  string  $ability
     * @param  \Illuminate\Database\Eloquent\Model|string|null  $model
     * @return array
     */
    protected function compileAbilityIdentifiers($ability, $model)
    {
        if (is_null($model)) {
            return [strtolower($ability)];
        }

        return $this->compileModelAbilityIdentifiers($ability, $model);
    }

    /**
     * Compile a list of ability identifiers that match the given model.
     *
     * @param  string  $ability
     * @param  \Illuminate\Database\Eloquent\Model|string|null  $model
     * @return array
     */
    protected function compileModelAbilityIdentifiers($ability, $model)
    {
        $model = $model instanceof Model ? $model : new $model;

        $identifier = strtolower($ability.'-'.$model->getMorphClass());

        if ( ! $model->exists) {
            return [$identifier];
        }

        return [$identifier, $identifier.'-'.$model->getKey()];
    }

    /**
     * Get the given user's roles.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $user
     * @return \Illuminate\Support\Collection
     */
    public function getRoles(Model $user)
    {
        $roles = $user->roles()->lists('name');

        return $roles;
    }

    /**
     * Get a list of the user's abilities.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $user
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAbilities(Model $user)
    {
        $query = Models::ability()->whereHas('roles', $this->getRoleUsersConstraint($user))
                                  ->orWhereHas('roles', function ($query) use($user) {
                                      $query->whereIn('id', $user->roles()->get()->pluck('child_role_id')->toArray());
                                  });

        $abilities = $query->orWhereHas('users', $this->getUserConstraint($user))->get();



        return $abilities;
    }

    /**
     * Constrain a roles query by the given user.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $user
     * @return \Closure
     */
    protected function getRoleUsersConstraint(Model $user)
    {
        return function ($query) use ($user) {
            $query->whereHas('users', $this->getUserConstraint($user));
        };
    }

    /**
     * Constrain a roles query by the given user.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $user
     * @return \Closure
     */
    protected function getRoleParent(Model $user)
    {
        return function ($query) use ($user) {
            $query->whereHas('users', $this->getUserConstraint($user));
        };
    }

    /**
     * Constrain a related query to the given user.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $user
     * @return \Closure
     */
    protected function getUserConstraint(Model $user)
    {
        return function ($query) use ($user) {
            $column = "{$user->getTable()}.{$user->getKeyName()}";

            $query->where($column, $user->getKey());
        };
    }


    /**
     *
     * Define new ability if its missing in database
     *
     * @param $user
     * @param $abilityName
     * @param $entities
     */
    protected function checkAndDefineAbility($user, $abilityName, $entities){
        if (env("APP_ENV") !== "production" || config('app.check_missing_abilities')) {
                /**
                 * Check if ability exists in table 'abilities', create new one if it does not exist
                 */
                if (is_string($abilityName) && count($entities)) {
                    foreach ($entities as $entity) {
                        /**
                         * Check for specific entity
                         */
                        if (is_object($entity) && isset($entity->id)) {
                            $ability = DB::table("abilities")
                                ->select('id')
                                ->where("name", "=", $abilityName)
                                ->where("entity_type", "=", get_class($entity))
                                ->where("entity_id", "=", $entity->id)
                                ->first();

                            if ($ability === null) {
                                DB::table('abilities')->insert(
                                    [
                                        'name' => $abilityName,
                                        'entity_id' => $entity->id,
                                        'entity_type' => get_class($entity),
                                        'created_at' => date("Y-m-d h:i:s"),
                                        'updated_at' => date("Y-m-d h:i:s"),
                                        'in_seeds' => 0
                                    ]
                                );
                            }
                        } /**
                         * Check for class ability
                         */
                        else if (is_string($entity)) {
                            $ability = DB::table("abilities")
                                ->select('id')
                                ->where("name", "=", $abilityName)
                                ->where("entity_type", "=", $entity)
                                ->first();

                            if ($ability === null) {
                                DB::table('abilities')->insert(
                                    [
                                        'name' => $abilityName,
                                        'entity_type' => $entity,
                                        'created_at' => date("Y-m-d h:i:s"),
                                        'updated_at' => date("Y-m-d h:i:s"),
                                        'in_seeds' => 0
                                    ]
                                );
                            }
                        }
                    }
                } /**
                 * Check for global ability
                 */
                else if (is_string($abilityName) && count($entities) == 0) {
                    $ability = DB::table("abilities")
                        ->select('id')
                        ->where("name", "=", $abilityName)
                        ->where("entity_type", "=", null)
                        ->where("entity_id", "=", null)
                        ->first();

                    if ($ability === null) {
                        DB::table('abilities')->insert(
                            [
                                'name' => $abilityName,
                                'created_at' => date("Y-m-d h:i:s"),
                                'updated_at' => date("Y-m-d h:i:s"),
                                'in_seeds' => 0
                            ]
                        );
                    }
                }
            }
    }
}
