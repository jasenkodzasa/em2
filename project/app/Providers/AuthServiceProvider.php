<?php

namespace App\Providers;

use Illuminate\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Bouncer;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {

        $this->registerPolicies($gate);

        $gate->before(function ($user, $abilityName, $entities) {
//            if (env("APP_ENV") !== "production" || config('app.check_abilities')) {
//                /**
//                 * Check if ability exists in table 'abilities', create new one if it does not exist
//                 */
//                if (is_string($abilityName) && count($entities)) {
//                    foreach ($entities as $entity) {
//                        /**
//                         * Check for specific entity
//                         */
//                        if (is_object($entity) && isset($entity->id)) {
//                            $ability = DB::table("abilities")
//                                ->select('id')
//                                ->where("name", "=", $abilityName)
//                                ->where("entity_type", "=", get_class($entity))
//                                ->where("entity_id", "=", $entity->id)
//                                ->first();
//
//                            if ($ability === null) {
//                                DB::table('abilities')->insert(
//                                    [
//                                        'name' => $abilityName,
//                                        'entity_id' => $entity->id,
//                                        'entity_type' => get_class($entity),
//                                        'created_at' => date("Y-m-d h:i:s"),
//                                        'updated_at' => date("Y-m-d h:i:s"),
//                                        'in_seeds' => 0
//                                    ]
//                                );
//                            }
//                        } /**
//                         * Check for class ability
//                         */
//                        else if (is_string($entity)) {
//                            $ability = DB::table("abilities")
//                                ->select('id')
//                                ->where("name", "=", $abilityName)
//                                ->where("entity_type", "=", $entity)
//                                ->first();
//
//                            if ($ability === null) {
//                                DB::table('abilities')->insert(
//                                    [
//                                        'name' => $abilityName,
//                                        'entity_type' => $entity,
//                                        'created_at' => date("Y-m-d h:i:s"),
//                                        'updated_at' => date("Y-m-d h:i:s"),
//                                        'in_seeds' => 0
//                                    ]
//                                );
//                            }
//                        }
//                    }
//                } /**
//                 * Check for global ability
//                 */
//                else if (is_string($abilityName) && count($entities) == 0) {
//                    $ability = DB::table("abilities")
//                        ->select('id')
//                        ->where("name", "=", $abilityName)
//                        ->where("entity_type", "=", null)
//                        ->where("entity_id", "=", null)
//                        ->first();
//
//                    if ($ability === null) {
//                        DB::table('abilities')->insert(
//                            [
//                                'name' => $abilityName,
//                                'created_at' => date("Y-m-d h:i:s"),
//                                'updated_at' => date("Y-m-d h:i:s"),
//                                'in_seeds' => 0
//                            ]
//                        );
//                    }
//                }
//            }
        });


    }
}
