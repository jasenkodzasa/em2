<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Bouncer;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();
        Bouncer::allow($user)->to('delete', \App\User::class);
        //dd($user->getAbilities());

        return view('home');
    }

    public function test(Request $request){
        $user = Auth::user();
        //Bouncer::allow($user)->to('create');
        //dd($user->getAbilities());

        return view('test');
    }
}
